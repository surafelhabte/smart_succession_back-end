<?php
use Restserver\Libraries\REST_Controller;
use Restserver\Libraries\REST;

defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/Format.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

class Applicant extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
  }

    public function __construct()
    {
        parent::__construct();
        $this->__resTraitConstruct();
        $this->load->library('Validate_Token');
        $this->load->model('Applicant_Model');
    }
 
    public function index_get($job_id)
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Applicant_Model->Gets($job_id);
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }

    }

    public function Detail_post()
    {
        $response = $this->validate_token->authenticateToken($this->input->request_headers());
        if ($response) {
            $result = $this->Applicant_Model->Detail($this->post());
            $this->response($result, REST::HTTP_OK);

        } else {
            $this->response(['Not authorized'], REST::HTTP_OK);

        }

    }
   
}
