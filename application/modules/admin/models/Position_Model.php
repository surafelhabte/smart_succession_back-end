<?php

   class Position_Model extends CI_Model
   {
       
        public function __construct()
        {
           parent::__construct();

           $this->recruit_Db=config_item('recruit_db');                
           $this->load->helper('uuid_gen');

        }

        public function Create($post){
            $this->form_validation->set_rules($this->validation())->set_data($post);
                        
            if($this->form_validation->run()) {
                $post['id'] = uuid_gen();

                $programs = $post['programs'];
                unset($post['programs']);

                if(count($programs) > 0){
                    foreach($programs as &$value){
                        $value = [$value];
                        $value['training_id'] = $value[0];
                        $value['position_id'] = $post['id'];
                        $value = array_slice($value,1);
                    }

                }
                
                $this->db->trans_begin();
                $this->db->insert('tbl_positions', $post);                
                $this->db->insert_batch('tbl_programs', $programs);                

                if ($this->db->trans_status() === true) {
                    $this->db->trans_commit();
                    return ['status'=>true, 'message' =>'position saved successfully.'];
    
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false, 'message' =>'unable to save the position.'];

                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
        }

        public function Update($post){   

            $id =  ['field' => 'id','label' => 'id','rules' => 'required'];
    
            $this->form_validation->set_rules(array_merge($this->validation(),$id))->set_data($post);
    
            if($this->form_validation->run()) {   
                $programs = $post['programs'];
                unset($post['programs']);

                $data_without_id = [];

                foreach($programs as $key => &$value){
                    $count = $this->db->from('tbl_programs')
                                      ->where(['position_id'=>$post['id'],'training_id'=>$value])
                                      ->count_all_results();

                    if($count === 0){

                      $value = [$value];
                      $value['training_id'] = $value[0];
                      $value['position_id'] = $post['id'];
                      $value = array_slice($value,1);

                      array_push($data_without_id, $value);
                    }  
                }

                $this->db->trans_begin();
                $this->db->update('tbl_positions',$post, ['id' => $post['id']]);
                count($data_without_id) > 0 ? $this->db->insert_batch('tbl_programs',$data_without_id) : null;

                if($this->db->trans_status() === TRUE){
                    $this->db->trans_commit();
                    return ['status'=>true,'message'=>'position updated successfully.'];
        
                } else {
                    $this->db->trans_rollback();
                    return ['status'=>false,'message'=>'unable to update position.'];
                }

            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }
            
        }
    
        public function Gets(){
            return $this->db->select("pos.id,jo.position")
                            ->from("tbl_positions as pos")
                            ->join("$this->recruit_Db.Jobs as jo","jo.id = pos.job_id")
                            ->get()->result(); 
        }
     
        public function Get($id){ 
            $result = $this->db->get_where("tbl_positions", ["id" => $id])->row();

            $result->programs = $this->db->get_where("tbl_programs", ["position_id" => $id])->result();

            return $result;

        }
      
        public function Delete($id){
            
            $this->db->trans_begin();
            $this->db->delete('tbl_positions', ['id' => $id]);

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                return ['status'=>true, 'message' =>'position deleted successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false, 'message' =>'unable to delete the position.'];

            }
        }

        public function Remove($post){

            $this->db->trans_begin();
            $this->db->delete('tbl_programs', $post);

            if ($this->db->trans_status() === true) {
                $this->db->trans_commit();
                return ['status'=>true, 'message' =>'program deleted successfully.'];

            } else {
                $this->db->trans_rollback();
                return ['status'=>false, 'message' =>'unable to delete the program.'];

            }
        }

        //Validation
        private function validation(){
            return [
                ['field' => 'job_id','label' => 'job','rules' => 'required']
            ];
        }

    }
