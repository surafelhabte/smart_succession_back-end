<?php

   class Applicant_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();

           $this->recruit_Db=config_item('recruit_db');                
           $this->core_Db=config_item('core_db');                
           $this->learning_Db=config_item('learning_db');                
        }

        public function Gets($vac_id){ 
            $select = 'emp.employee_id,concat_ws(" ",first_name,middle_name,last_name) as full_name,
                        pos.position,department_name,emp.location,app.created_date,vac.JobId';

            return $this->db->select($select)
                            ->from("$this->recruit_Db.Application as app")
                            ->join("$this->recruit_Db.Vacancy as vac","app.VacancyId = vac.id")
                            ->join("$this->core_Db.employee_data as emp","app.employee_id = emp.employee_id")
                            ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                            ->join("$this->core_Db.department as dep","pos.department_id = dep.id")
                            ->order_by("app.created_date","DESC")
                            ->where(["vac.id" => $vac_id])
                            ->get()->result(); 
        }

        public function Detail($post){
            $select = 'emp.employee_id,concat_ws(" ",first_name,middle_name,last_name) as full_name,
                        pos.position,department_name,location,date_of_employment';

            $result =  $this->db->select($select)
                                ->from("$this->core_Db.employee_data as emp")
                                ->join("$this->core_Db.position as pos","pos.id = emp.position_id")
                                ->join("$this->core_Db.department as dep","pos.department_id = dep.id")
                                ->where(["emp.employee_id" => $post['emp_id']])
                                ->get()->row(); 

            $select = "certificate,start_date,end_date,subject,education_status,school";                     

            $result->education = $this->db->select($select)
                                          ->order_by("end_date")  
                                          ->get_where("$this->core_Db.education", ["employee_id" => $post['emp_id']])
                                          ->result();

            $select = "place_of_work,from_date,to_date,position,ROUND(DATEDIFF(to_date,from_date)/30,2) as Months";                              
                                          
            $result->experience = $this->db->select($select)
                                           ->order_by("to_date")  
                                           ->get_where("$this->core_Db.experience", ["employee_id" => $post['emp_id']])
                                           ->result(); 
                                                                    
            $select = "from_position,to_position,from_date,to_date,employee_status";                              
                                          
            $result->career_history = $this->db->select($select)
                                               ->order_by("to_date")  
                                               ->get_where("$this->core_Db.employee_status", ["employee_id" => $post['emp_id']])
                                               ->result(); 

            
            $select = "IFNULL(title,training) as training,date,cert.created_at";                              
                                          
            $result->training = $this->db->select($select)
                                        ->from("$this->learning_Db.tbl_certification as cert")
                                        ->join("$this->learning_Db.tbl_enrollment as enr", "enr.id = cert.enrollment_id","left")
                                        ->join("$this->learning_Db.tbl_program as pro", "pro.id = enr.program_id","left")
                                        ->join("$this->learning_Db.tbl_training as tra", "tra.id = pro.training_id","left")
                                        ->order_by("cert.created_at")  
                                        ->where(["cert.employee_id" => $post['emp_id']])
                                        ->get()->result();                    
            
            $select = 'pos.id,Position,Department,Field_Of_Study,Work_Experience,Special_Work_Experience,Special_Year_Of_Experience,Details';

            $result_2 = $this->db->select($select)
                                 ->from("tbl_positions as pos")
                                 ->join("$this->recruit_Db.Jobs as jo", "pos.job_id = jo.id")
                                 ->where("job_id = $post[job_id]")
                                 ->get()->row();

            $result_2->programs = $this->db->select("title")
                                           ->from("tbl_programs as pro")
                                           ->join("$this->learning_Db.tbl_training as tra","tra.id = pro.training_id")
                                           ->where(["position_id" => $result_2->id])
                                           ->get()->result();

            $result->detail = $result_2;
                                           
            return $result;                                   
                                                      
        }

   }