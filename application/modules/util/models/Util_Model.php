<?php

   class Util_Model extends CI_Model
   {
        public function __construct()
        {
           parent::__construct();
           $this->recruit_Db=config_item('recruit_db');                
           $this->learning_Db=config_item('learning_db');                

        }
                
        public function Training()
        {
          return $this->db->select("id,title")
                          ->get("$this->learning_Db.tbl_training")
                          ->result();
        }

        public function Job($id)
        {
            if(!is_null($id)){
               return $this->db->select("id,Position")->get("$this->recruit_Db.Jobs")->result();
                           
            } else {
               return $this->db->select("jo.id,Position")
                              ->from("$this->recruit_Db.Jobs as jo") 
                              ->where("jo.id not in (select job_id from tbl_positions)")
                              ->get()->result();
            }

        }

        public function Vacancy()
        {
            $select = 'vac.id, concat(Title," => ",DATE_FORMAT(Created_date, "%Y-%m-%d")) as title';

            return $this->db->select($select)
                            ->from("$this->recruit_Db.Vacancy as vac") 
                            ->join("$this->recruit_Db.Jobs as jo", "vac.JobId = jo.id")
                            ->where("jo.For_Successors = '1'")  
                            ->get()->result();      
        }
            
   }
