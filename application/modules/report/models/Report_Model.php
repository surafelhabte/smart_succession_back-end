<?php

   class Report_Model extends CI_Model
   {
       public function __construct()
       {
           parent::__construct();
           $this->core_Db=config_item('core_db');
       }

        public function Submission($post)
        {
            $id = ['field' => 'table','label' => 'table','rules' => 'required'];
            
            $post['fiscal_year'] ?? date('Y');

            $table = $post['table'];                  

            $result =  $this->db->select("department_name,submission_status,count(dep.id) as total_employee,dep.id")
                            ->from("$this->core_Db.department as dep")
                            ->join("$this->core_Db.position as pos","pos.department_id = dep.id")
                            ->join("$this->core_Db.employee_data as emp","emp.position_id = pos.id")
                            ->join("tbl_apa as apa","apa.employee_id = emp.employee_id")
                            ->group_by(["dep.id","submission_status"])
                            ->where(["Year(apa.created_at)"=>$post['fiscal_year']])
                            ->get()->result(); 

                            return $result;

            $this->form_validation->set_rules($id)->set_data($post);

            if($this->form_validation->run()) {


            } else {
                return ['status' => false, 'message' => implode('\n', $this->form_validation->error_array())];

            }         
        }
    
   }
